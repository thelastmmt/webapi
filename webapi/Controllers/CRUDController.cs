﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using webapi.Models;

namespace webapi.Controllers
{
    public class CrudController : Controller
    {
        public ActionResult Index()
        {
            IEnumerable<DocumentsViewModel> documents = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44302/api/");
                var responseTask = client.GetAsync("Documents");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<DocumentsViewModel>>();
                    readTask.Wait();

                    documents = readTask.Result;
                    System.Diagnostics.Debug.WriteLine(documents);
                }
                else //web api sent error response 
                {
                    //log response status here..
                    System.Diagnostics.Debug.WriteLine(result);
                    documents = Enumerable.Empty<DocumentsViewModel>();

                    ModelState.AddModelError(string.Empty, result.ToString());
                }
            }
            return View(documents);
        }
    }
}
